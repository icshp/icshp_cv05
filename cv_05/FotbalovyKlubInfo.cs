﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cv_05
{
    class FotbalovyKlubInfo
    {
        readonly static int Pocet = 6;

        public static string DejNazev(FotbalovyKlub value) {
            switch (value)
            {
                case FotbalovyKlub.FcPorto:
                    return "FC Porto";
                case FotbalovyKlub.RealMadrid:
                    return "Real Madrid";
                default:
                    return value.ToString();
            }
        }

        public static FotbalovyKlub[] ZiskejKluby()
        {
            return new FotbalovyKlub[] {
            FotbalovyKlub.None,
            FotbalovyKlub.FcPorto,
            FotbalovyKlub.Arsenal,
            FotbalovyKlub.RealMadrid,
        FotbalovyKlub.Chelsea,
        FotbalovyKlub.Barcelona};
        }
    }
}
