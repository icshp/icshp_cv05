﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cv_05
{
    public class Hrac
    {
        public string Jmeno { get; set;}
        public FotbalovyKlub Klub { get; set;}
        public int GolPocet { get; set; }

        public Hrac(string jmeno, FotbalovyKlub klub)
        {
            Jmeno = jmeno;
            Klub = klub;
            GolPocet = 0;
        }

        public override string ToString()
        {
            return "Hráč: " + Jmeno + ", Klub: " + FotbalovyKlubInfo.DejNazev(Klub) + ", Pocet gólů:" + GolPocet;
        }
    }
}
