﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cv_05
{

    public delegate void UpdatedCountEventHandler(object sender, EventArgs e);

    public class Hraci
    {
        public event UpdatedCountEventHandler UpdatedCount;
        public int Pocet { get { return hraci.Length; } }
        Hrac[] hraci = new Hrac[0];

        public void OnUpdatedCount()
        {
            UpdatedCountEventHandler handler = UpdatedCount;
            if (handler != null)
                handler(this, new EventArgs());
        }

        public void Vymaz(Hrac hrac) {
            List<Hrac> temporaryList = hraci.ToList<Hrac>();
            temporaryList.Remove(hrac);
            hraci = temporaryList.ToArray();
            OnUpdatedCount();
        }

        public void Pridej(Hrac hrac) {
            List<Hrac> temporaryList = hraci.ToList<Hrac>();
            temporaryList.Add(hrac);
            hraci = temporaryList.ToArray();
            OnUpdatedCount();
        }

        public Hrac this[int index]
        {
            get => hraci[index];
        }

        public void NajdiNejlepsiKluby(out FotbalovyKlub[] kluby, out int pocetGolu) {
            Dictionary<FotbalovyKlub, int> score = new Dictionary<FotbalovyKlub, int>();
            foreach (FotbalovyKlub item in FotbalovyKlubInfo.ZiskejKluby())
            {
                score.Add(item, 0);
            }

            foreach (Hrac item in hraci)
            {
                score[item.Klub] += item.GolPocet;
            }

            pocetGolu = score.Values.Max();
            List<FotbalovyKlub> maxKluby = new List<FotbalovyKlub>();
            if (pocetGolu > 0)
            {
                foreach (var item in score)
                {
                    if (item.Value == pocetGolu)
                    {
                        maxKluby.Add(item.Key);
                    }
                }
            }
            kluby = maxKluby.ToArray();

        }

    }
}
