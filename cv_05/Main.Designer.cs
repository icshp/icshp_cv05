﻿namespace cv_05
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxHraci = new System.Windows.Forms.ListBox();
            this.buttonPridejHrace = new System.Windows.Forms.Button();
            this.buttonVymaz = new System.Windows.Forms.Button();
            this.buttonUprav = new System.Windows.Forms.Button();
            this.buttonNejKlub = new System.Windows.Forms.Button();
            this.buttonRegistrovat = new System.Windows.Forms.Button();
            this.buttonZrusit = new System.Windows.Forms.Button();
            this.buttonKonec = new System.Windows.Forms.Button();
            this.listBoxPocetHracu = new System.Windows.Forms.ListBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.labelState = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxHraci
            // 
            this.listBoxHraci.FormattingEnabled = true;
            this.listBoxHraci.Location = new System.Drawing.Point(12, 12);
            this.listBoxHraci.Name = "listBoxHraci";
            this.listBoxHraci.Size = new System.Drawing.Size(516, 199);
            this.listBoxHraci.TabIndex = 0;
            // 
            // buttonPridejHrace
            // 
            this.buttonPridejHrace.Location = new System.Drawing.Point(537, 13);
            this.buttonPridejHrace.Name = "buttonPridejHrace";
            this.buttonPridejHrace.Size = new System.Drawing.Size(102, 23);
            this.buttonPridejHrace.TabIndex = 1;
            this.buttonPridejHrace.Text = "Přidat hráče";
            this.buttonPridejHrace.UseVisualStyleBackColor = true;
            this.buttonPridejHrace.Click += new System.EventHandler(this.buttonPridejHrace_Click);
            // 
            // buttonVymaz
            // 
            this.buttonVymaz.Location = new System.Drawing.Point(537, 42);
            this.buttonVymaz.Name = "buttonVymaz";
            this.buttonVymaz.Size = new System.Drawing.Size(102, 23);
            this.buttonVymaz.TabIndex = 2;
            this.buttonVymaz.Text = "Vymaž hráče";
            this.buttonVymaz.UseVisualStyleBackColor = true;
            this.buttonVymaz.Click += new System.EventHandler(this.buttonVymaz_Click);
            // 
            // buttonUprav
            // 
            this.buttonUprav.Location = new System.Drawing.Point(537, 71);
            this.buttonUprav.Name = "buttonUprav";
            this.buttonUprav.Size = new System.Drawing.Size(102, 23);
            this.buttonUprav.TabIndex = 3;
            this.buttonUprav.Text = "Uprav hráče";
            this.buttonUprav.UseVisualStyleBackColor = true;
            this.buttonUprav.Click += new System.EventHandler(this.buttonUprav_Click);
            // 
            // buttonNejKlub
            // 
            this.buttonNejKlub.Location = new System.Drawing.Point(537, 100);
            this.buttonNejKlub.Name = "buttonNejKlub";
            this.buttonNejKlub.Size = new System.Drawing.Size(102, 23);
            this.buttonNejKlub.TabIndex = 4;
            this.buttonNejKlub.Text = "Nejlepší klub";
            this.buttonNejKlub.UseVisualStyleBackColor = true;
            this.buttonNejKlub.Click += new System.EventHandler(this.buttonNejKlub_Click);
            // 
            // buttonRegistrovat
            // 
            this.buttonRegistrovat.Location = new System.Drawing.Point(537, 129);
            this.buttonRegistrovat.Name = "buttonRegistrovat";
            this.buttonRegistrovat.Size = new System.Drawing.Size(102, 23);
            this.buttonRegistrovat.TabIndex = 5;
            this.buttonRegistrovat.Text = "Registrovat\r\n";
            this.buttonRegistrovat.UseVisualStyleBackColor = true;
            this.buttonRegistrovat.Click += new System.EventHandler(this.buttonRegistrovat_Click);
            // 
            // buttonZrusit
            // 
            this.buttonZrusit.Location = new System.Drawing.Point(537, 158);
            this.buttonZrusit.Name = "buttonZrusit";
            this.buttonZrusit.Size = new System.Drawing.Size(102, 23);
            this.buttonZrusit.TabIndex = 6;
            this.buttonZrusit.Text = "Zrušit registraci";
            this.buttonZrusit.UseVisualStyleBackColor = true;
            this.buttonZrusit.Click += new System.EventHandler(this.buttonZrusit_Click);
            // 
            // buttonKonec
            // 
            this.buttonKonec.Location = new System.Drawing.Point(537, 187);
            this.buttonKonec.Name = "buttonKonec";
            this.buttonKonec.Size = new System.Drawing.Size(102, 23);
            this.buttonKonec.TabIndex = 7;
            this.buttonKonec.Text = "Konec";
            this.buttonKonec.UseVisualStyleBackColor = true;
            this.buttonKonec.Click += new System.EventHandler(this.buttonKonec_Click);
            // 
            // listBoxPocetHracu
            // 
            this.listBoxPocetHracu.FormattingEnabled = true;
            this.listBoxPocetHracu.Location = new System.Drawing.Point(12, 217);
            this.listBoxPocetHracu.Name = "listBoxPocetHracu";
            this.listBoxPocetHracu.Size = new System.Drawing.Size(627, 95);
            this.listBoxPocetHracu.TabIndex = 8;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.labelState});
            this.statusStrip1.Location = new System.Drawing.Point(0, 301);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(649, 22);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(50, 17);
            this.toolStripStatusLabel1.Text = "Výpis je:";
            // 
            // labelState
            // 
            this.labelState.ForeColor = System.Drawing.Color.Red;
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(157, 17);
            this.labelState.Text = "Vypnutý! (Registruj handler!)";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 323);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.listBoxPocetHracu);
            this.Controls.Add(this.buttonKonec);
            this.Controls.Add(this.buttonZrusit);
            this.Controls.Add(this.buttonRegistrovat);
            this.Controls.Add(this.buttonNejKlub);
            this.Controls.Add(this.buttonUprav);
            this.Controls.Add(this.buttonVymaz);
            this.Controls.Add(this.buttonPridejHrace);
            this.Controls.Add(this.listBoxHraci);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.Text = "Liga Mistrů";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxHraci;
        private System.Windows.Forms.Button buttonPridejHrace;
        private System.Windows.Forms.Button buttonVymaz;
        private System.Windows.Forms.Button buttonUprav;
        private System.Windows.Forms.Button buttonNejKlub;
        private System.Windows.Forms.Button buttonRegistrovat;
        private System.Windows.Forms.Button buttonZrusit;
        private System.Windows.Forms.Button buttonKonec;
        private System.Windows.Forms.ListBox listBoxPocetHracu;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel labelState;
    }
}

