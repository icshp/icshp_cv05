﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cv_05
{
    public partial class Main : Form
    {
        Hraci hraci;

        public Main()
        {
            InitializeComponent();
            hraci = new Hraci();
        }

        private void PocetZmenen(object sender, EventArgs e)
        {
            listBoxHraci.Items.Clear();
            for (int i = 0; i < hraci.Pocet; i++)
            {
                listBoxHraci.Items.Add(hraci[i]);
            }
            listBoxPocetHracu.Items.Add("Změna počtu hráčů: " + hraci.Pocet);
        }

        private void buttonPridejHrace_Click(object sender, EventArgs e)
        {
            PridaniHrace pridejHrace = new PridaniHrace(null);
            pridejHrace.ShowDialog();
            if(pridejHrace.DialogResult == DialogResult.OK)
            {
                hraci.Pridej(pridejHrace.hrac);
                MessageBox.Show("Byl přidán: " + pridejHrace.hrac.ToString(), "Informace", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonNejKlub_Click(object sender, EventArgs e)
        {
            Score score = new Score(hraci);
            score.Show();
        }

        private void buttonVymaz_Click(object sender, EventArgs e)
        {
            hraci.Vymaz((Hrac) listBoxHraci.SelectedItem);
        }

        private void buttonUprav_Click(object sender, EventArgs e)
        {
            if ((Hrac)listBoxHraci.SelectedItem != null) { 
                PridaniHrace pridejHrace = new PridaniHrace((Hrac)listBoxHraci.SelectedItem);
                pridejHrace.ShowDialog();
                if (pridejHrace.DialogResult == DialogResult.OK)
                {
                    hraci.Vymaz((Hrac)listBoxHraci.SelectedItem);
                    hraci.Pridej(pridejHrace.hrac);
                    MessageBox.Show("Byl upraven: " + pridejHrace.hrac.ToString(), "Informace", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void buttonKonec_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonRegistrovat_Click(object sender, EventArgs e)
        {
            labelState.Text = "Zapnutý. Handler registrován!";
            labelState.ForeColor = Color.Green;
            hraci.UpdatedCount += PocetZmenen;
        }

        private void buttonZrusit_Click(object sender, EventArgs e)
        {
            labelState.Text = "Vypnutý. Handler neregistrován!";
            labelState.ForeColor = Color.Red;
            hraci.UpdatedCount -= PocetZmenen;
        }
    }
}
